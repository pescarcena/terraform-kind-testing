# terraform-kind-testing

 ***Prueba de despliegue de aplicación en cluster de kubenetes (kind)***

 En caso de tener sistema operativo windows, usaremos una máquina en vagrant con virtual box con centos como SO

- Realizar el despliegue de la vm vagrant:

```shell
vagrant up
````

para usuarios Mac:

```shell
brew install kind
brew install terraform
brew install kubernetes-cli
```

- Instalar cluster kind con el siguiente comando:

```shell
kind create cluster \
--name gc-hcmc-kubernetes-demo \
--image kindest/node:v1.16.9 \
--config=kind-configuration.yaml
```

## Practica: Despliegue de ingress controller y deployment de aplicación dummy con terraform en k8s

- Aplicar script para configuración de variables:

```shell
. preparation_terraform.sh
```

- Instalar modulos y dependencias en terraform:

```shell
terraform init
```

- Aplicar plan de despliegue de recursos:

```shell
terraform plan
```

- Aplicar despliegue de despliegue de recursos y confirmar:

```shell
terraform apply
```

- Listar pods en namespace default y tests donde se verifica la instalación de los recursos

```kubectl get pods -n test
kubectl get pods -n default
```

- Alterar la configuración del despliegue de la aplicación en el archivo de configuración de terraform en el archivo main.tf cambiar los limites a (0.7):

```hcl
            limits {
              cpu    = "0.7"
              memory = "512Mi"
            }
```

- Aplicar de nuevo el despliegue observando los cambios:

```shell
terraform apply
```

_ Alterar la configuración en el deployment en k8s replicas a (2):

```shell
kubectl edit deploy terraform-example -n test
```

```yaml
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  ```

  - Aplicar de nuevo el despliegue observando los cambios:

```shell
terraform apply
```