provider "kubernetes" {
  load_config_file       = "false"
  host                   = var.k8s_host
  client_certificate     = base64decode(file(var.cert))
  client_key             = base64decode(file(var.key))
  cluster_ca_certificate = base64decode(file(var.cacert))
}

resource "kubernetes_namespace" "example" {
  metadata {
    name = "test"
  }
}

resource "kubernetes_deployment" "example" {
  metadata {
    name = "terraform-example"
    namespace = "test"
    labels = {
      app = "test"
      test = "test"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "test"
        test = "test"
      }
    }

    template {
      metadata {
        labels = {
          test = "test"
          app = "test"
        }
      }

      spec {
        container {
          image = "nginx:1.7.8"
          name  = "example"

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

provider "helm" {
  version = "1.2.0"
  kubernetes {
    load_config_file       = "false"
    host                   = var.k8s_host
    client_certificate     = base64decode(file(var.cert))
    client_key             = base64decode(file(var.key))
    cluster_ca_certificate = base64decode(file(var.cacert))
  }
}

resource "helm_release" "nginx-ingress" {
  name       = "test"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "nginx-ingress"
  version    = "1.24.4"
  namespace  = "default"
  skip_crds  = "true"
  timeout    = 1500
  wait       = false

  set {
      name = "controller.replicaCount"
      value = "1"
  }
  values = [
    file("templates/values.yml")
  ]
}