#!/bin/bash
kubectl config view --minify --flatten --context=kind-gc-hcmc-kubernetes-demo > $HOME/kubeconfig_test
export TF_VAR_k8s_host=$(grep server $HOME/.kube/config | cut -d : -f 2,3,4 | tr " \t" "\n" | awk 'NF')
export TF_VAR_cert=$HOME/kubeconfig.crt
export TF_VAR_key=$HOME/kubeconfig.key
export TF_VAR_cacert=$HOME/kubeconfig-ca.crt
cat $HOME/kubeconfig_test | grep certificate-authority-data | cut -d ":" -f 2 | tr " \t" "\n" | awk 'NF' > $HOME/kubeconfig-ca.crt
cat $HOME/kubeconfig_test | grep client-key-data | cut -d ":" -f 2 | tr " \t" "\n" | awk 'NF' > $HOME/kubeconfig.key
cat $HOME/kubeconfig_test | grep client-certificate-data | cut -d ":" -f 2 | tr " \t" "\n" | awk 'NF' > $HOME/kubeconfig.crt